from django.conf.urls import patterns, url, include
from rest_framework.urlpatterns import format_suffix_patterns
from rest_api.profile import views as profile_views


urlpatterns = patterns(
    '',
    url(r'^users/(?P<email>[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z-0-9]{2,4})/$',
        profile_views.UserProfile.as_view(), name='user_profile'),

)

urlpatterns = format_suffix_patterns(urlpatterns)
