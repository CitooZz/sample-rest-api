from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save
from django.utils.html import strip_tags
from django.template.defaultfilters import slugify




class Profile(models.Model):

    class Meta:
        app_label = "profile"

    user = models.OneToOneField(User, unique=True, verbose_name=_("user"), related_name="profile")
    full_name = models.CharField(_("Full Name"), max_length=30, blank=True)
    head_line = models.CharField(_("Head Line"), max_length=200, blank=True)
    company = models.CharField(_("Company"), max_length=200, blank=True)
    summary = models.TextField(_("Summary"), blank=True)
    education = models.TextField(_("Education"), blank=True)
    experiences = models.TextField(_("Experience"), blank=True)
    website = models.URLField(_("Website"), blank=True)
    linkedin_profile_url = models.URLField(
        _("LinkedIn Profile Page"), blank=True)

    # LinkedIn Id & Token
    linkedin_id = models.CharField(max_length=200, blank=True)
    linkedin_token = models.TextField(blank=True)

    # Facebook Id & Token
    facebook_id = models.CharField(max_length=20, blank=True)
    facebook_token = models.TextField(blank=True)

    daily_notification = models.BooleanField(default=False)
    terms_accepted = models.BooleanField(default=True)
    email_notification_enabled = models.BooleanField(default=True)

    def __unicode__(self):
        return self.full_name

    @models.permalink
    def get_absolute_url(self):
        return "profile_detail", [self.user.username]

    @property
    def clean_summary(self):
        return strip_tags(self.summary)

    @property
    def clean_experiences(self):
        return strip_tags(self.experiences)

    @property
    def clean_education(self):
        return strip_tags(self.education)


def create_profile_for_new_user(sender, instance=None, **kwargs):
    if instance is None:
        return

    profile, created = Profile.objects.get_or_create(user=instance)
    if created:
        profile.full_name = "%s %s" % (instance.first_name, instance.last_name)
        profile.save()

post_save.connect(create_profile_for_new_user, sender=User)


def generate_username(first_name, last_name):
    for x in xrange(200):
        username = "%s-%s" % (
            slugify(first_name + ' ' + last_name), x)
        if not User.objects.filter(username=username).exists():
            break
    return username