from rest_framework.generics import get_object_or_404, RetrieveUpdateDestroyAPIView
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth.models import User
from rest_api.profile.models import Profile
from serializers import ProfileSerializer, ProfileEditSerializer


class UserProfile(RetrieveUpdateDestroyAPIView):
    model = Profile

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return ProfileSerializer
        return ProfileEditSerializer

    def get(self, request, email):
        user = get_object_or_404(User, email=email)
        return Response(ProfileSerializer(user.profile).data)

    def put(self, request, email):
        user = get_object_or_404(User, email=email)
        profile = ProfileEditSerializer(user.profile, data=request.DATA)
        if profile.is_valid():
            profile.object.user = user
            profile.save()
            return Response(profile.data, status=status.HTTP_201_CREATED)
        return Response(profile.errors, status=status.HTTP_400_BAD_REQUEST)
