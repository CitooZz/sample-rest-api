from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

from rest_framework import serializers
from rest_api.profile.models import Profile


class UserSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField("get_full_name")
    api_endpoint = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ("id", "username", "name", "api_endpoint")

    def get_full_name(self, obj):
        return obj

    def get_api_endpoint(self, obj):
        if obj.email:
            return reverse("api:user_profile", args=[obj.email])
        return ''


class ProfileSerializer(serializers.ModelSerializer):

    """ Serializer for profile """
    user = UserSerializer()
    summary = serializers.ReadOnlyField(source="clean_summary")
    experiences = serializers.ReadOnlyField(source="clean_experiences")
    education = serializers.ReadOnlyField(source="clean_education")

    class Meta:
        model = Profile
        exclude = ("linkedin_id", "linkedin_token",
                   "facebook_id", "facebook_token")


class ProfileEditSerializer(serializers.ModelSerializer):

    class Meta:
        model = Profile
        exclude = ("user", "linkedin_id", "linkedin_token",
                   "facebook_id", "facebook_token")


class CreateUserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        write_only=True,
        min_length=8,
        error_messages={
            "blank": "Password cannot be empty",
            "min_length": "Password is too short. Make sure it has at least 8 characters."
        }
    )

    class Meta:
        model = User
        fields = ("email", "first_name", "last_name", "password", )

    def validate_email(self, data, email):
        email = data.get(email)
        qs = User.objects.filter(email__exact=email)
        if qs.exists():
            raise serializers.ValidationError("Email already exist.")

        return data
